resource "hcloud_floating_ip" "matrix" {
  type              = "ipv4"
  name              = "matrix"
  home_location     = var.location
  delete_protection = true
  labels = {
    type = "matrix"
  }
}

resource "hcloud_floating_ip_assignment" "matrix" {
  floating_ip_id = hcloud_floating_ip.matrix.id
  server_id      = hcloud_server.matrix.id
}
