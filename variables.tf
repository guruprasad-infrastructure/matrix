variable "hcloud_token" {
  sensitive = true
}

variable "location" {
  default = "nbg1"
}

variable "server_type" {
  default = "cx21"
}

variable "os_type" {
  default = "debian-11"
}

variable "ssh_key_name" {
  default = "ssh_key"
}

variable "ssh_key_public_key" {
  default = "~/.ssh/id_rsa.pub"
}

variable "instances" {
  default = 1
}

variable "namecheap_user_name" {
}

variable "namecheap_api_key" {
}

variable "namecheap_matrix_base_domain" {
}
