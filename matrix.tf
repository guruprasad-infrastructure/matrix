resource "hcloud_server" "matrix" {
  name               = "matrix.${var.namecheap_matrix_base_domain}"
  image              = var.os_type
  location           = var.location
  server_type        = var.server_type
  ssh_keys           = [hcloud_ssh_key.default.id]
  backups            = true
  delete_protection  = true
  rebuild_protection = true
  labels = {
    type = "matrix"
  }
}
