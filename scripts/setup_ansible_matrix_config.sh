#!/bin/bash

set -eu

DEPLOYMENT_REPO="matrix-docker-ansible-deploy"

echo "Cleaning up any existing configuration"
rm -rf "$DEPLOYMENT_REPO/inventory/host_vars/*"
rm -f "$DEPLOYMENT_REPO/inventory/hosts"

echo "Seting up and copying the configuration files to the correct locations"
mkdir -p "$DEPLOYMENT_REPO/inventory/host_vars/matrix.$1"
cp ansible-config/vars.yml "$DEPLOYMENT_REPO/inventory/host_vars/matrix.$1/"
sed -i "/matrix_domain:/s/.*/matrix_domain: $1/" "$DEPLOYMENT_REPO/inventory/host_vars/matrix.$1/vars.yml"
cp $DEPLOYMENT_REPO/{examples,inventory}/hosts
sed -i "s/<your-domain>/$1/; s/<your-server's external IP address>/$2/" $DEPLOYMENT_REPO/inventory/hosts

echo "Setting up a virtualenv environment and installing ansible"
rm -rf venv
python -m venv venv
venv/bin/pip install 'ansible==4.10.0' --quiet 2>/dev/null
