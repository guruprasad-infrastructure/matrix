output "matrix_vm_address" {
  description = "Matrix VM IP address"
  value       = hcloud_server.matrix.ipv4_address
}

output "matrix_floating_ip_address" {
  description = "Floating IP address"
  value       = hcloud_floating_ip.matrix.ip_address
}
