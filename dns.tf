resource "namecheap_domain_records" "matrix" {
  domain = var.namecheap_matrix_base_domain

  record {
    hostname = "matrix"
    type     = "A"
    address  = hcloud_floating_ip.matrix.ip_address
    ttl      = 1800
  }

  record {
    hostname = "element"
    type     = "CNAME"
    address  = "matrix.${var.namecheap_matrix_base_domain}"
    ttl      = 1800
  }
}
